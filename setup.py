from cx_Freeze import setup, Executable

build_exe_options = {
      'packages': ['os', 'requests', 'queue', 'PyQt5', 'codecs'],
}

options = {
      'build_exe': build_exe_options,
      'py2exe': {'bundle_files': 1, 'compressed': True}
}

setup(name = 'QTLauncher',
      version = '0.2',
      description = 'PyQT-based launcher for clients that do not include a Desktop launcher.',
      options = options,
      executables = [Executable('main.py')])
